# Dynamic Back-up Workers for distributed machine learning: 

This work aims to the dynamically select the number of backup workers in [Parameter Sever architecture](https://www.cs.cmu.edu/~muli/file/parameter_server_osdi14.pdf) to improve the convergence speed.
The idea of backup workers was first introduced in the [paper](https://arxiv.org/abs/1604.00981) from Google researchers for synchronous Parameter Server to mitigate the straggler problem.
In their work, the number of the backup workers is constant. Their implementation is based on the [SyncReplicasOptimizer package](https://www.tensorflow.org/api_docs/python/tf/compat/v1/train/SyncReplicasOptimizer) of TensorFlow 1.0.
Unfortunately, the backup workers implemnetation is no longer available in Tensorflow 2.0. 
Instead, higher level [API](https://www.tensorflow.org/api_docs/python/tf/distribute/experimental/ParameterServerStrategy) is provided for a general Parameter Server strategy, but its internal working is not visible.
More details on this work could be found in the relevant [paper](https://arxiv.org/abs/2004.14696).

Here, we implement back-up workers scheme (DBW) in *PyTorch* framework and use *MPI* backend to synchronize the communication among the parameter server and the workers. 
The implementation details and the **pseudo-code** for DBW could be found in [this document](https://gitlab.inria.fr/chxu/dbw/blob/master/Implementation_Details.pdf).
The machine learning problem considered is the image classification task on MNIST dataset.
The model is a neural network with 2 convolutional layers and 2 linear layers. 

## Running requirements: 
*  Python
*  OpenMpi
*  PyTorch installed from the [source](https://github.com/pytorch/pytorch?source=post_page---------------------------) with MPI backend 
*  Cluster environment with GPUs


## Usage:
1.  Example of command: mpirun -n 17 --oversubscribe python maindynvirtual.py --k 16 --D 5 --times 1 --ngpu 4 --nproc_per_gpu 4 --bsz 500 --lr 0.08 --T 120 --distribution exponential
2.  Explanation of options of the code:
    *  k: number of workers
    *  n: number of workers + 1 (parameter server)
    *  times: the number of runs
    *  ngpu: the number of gpu in the cluster
    *  nproc_per_gpu: the number of the workers put to the same gpu
    *  bsz: batch size
    *  lr: learning rate
    *  T: the number of iterations
    *  distribution: the time distribution of round trip time
3. Output information: 
    *  Print on the screen: 
        1. the dynamic number of gradients aggregated, the index of the fastest workers considered, the local loss aggregated at Parameter Server side.
        2. The time estimator, the gain over time, the prediction of number of gradients to aggregate at the next iteration.
    *  Save to file: the estimates of the norm of gradients, the variance of gradients, the dynamic k, time stamp, local loss of every iteration.


## Contribution:
Joint work with Giovanni NEGLIA and Nicola SEBASTIANELLI