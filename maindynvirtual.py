#!/usr/bin/env python
import argparse
import os
import time
import torch
import torch.distributed as dist
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import pandas as pd
import numpy as np
from source import resnet
from source import read_data
import pickle
import copy
from math import ceil
from random import Random
from torch.multiprocessing import Process
from torch.autograd import Variable
from torchsummary import summary
from torchvision import datasets, transforms
from source.virtualtime.VirtualDelayNonResetMatrix import VirtualDelayPS
from source.DBW import check, DynamicChoice, GetWithLateInfo, Update, wait
from statistics import mean

def runps(num_workers, args, num_tries):
    data = args.data
    lr0 = args.lr
    k = args.k
    modelfix = args.modelfix
    torch.manual_seed(1234)
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benckmark = False

    model = resnet.choosemodel(modelfix, data)
    optimizer = optim.SGD(model.parameters(), lr=lr0 )

    T = args.T
    testlayers = args.layers
    SendreqList = []
    RecvreqList = []
    gradients =[]
    gradients_to_add= []
    count_times =[]
    timeprint= [0]
    dynamick = []
    InfoList = []
    timeregister = []
    sendingtime = []
    ExpectGainL = []
    GradNormsquareL = []
    WithLateGradsL=[]
    Losstoprint = []
    VarL = []
    VarkL = []
    WithLateVarkL=[]
    ExpectedGainL = []
    Num_recentL = [16]
    vDelay = VirtualDelayPS(T+1, num_workers, args.distribution,"mean", False, num_tries )
    
    for w in range(num_workers):
        gradients.append([])
        timeregister.append([])
        count_times.append([])
        count = 0
        for layer in model.parameters():
            fdim = [layer.size()[0]+2]
            rdim = list(layer.size()[1:])
            fdim.extend(rdim)
            gradients[-1].append(torch.zeros(fdim))
            count_times[-1].append(0)
            count+=1
            if count>=testlayers: break
    count = 0  
    for layer in model.parameters():
        gradients_to_add.append(layer.data.clone())
        RecvreqList.append([])
        print("size",layer.data.size())
        for w in range(num_workers):
            RecvreqList[-1].append([])
          
        count += 1
        if count>=testlayers: break 
    t = 0
    flag = 0
    while t<=T:
        count = 0
        for layer in model.parameters():
            for w in range(num_workers):
                SendreqList.append( dist.isend(layer.data,w))
                if t == 0:
                    RecvreqList[count][w] = dist.irecv(gradients[w][count],w)
                   # print(f"ps waits for the {count} th gradients from worker {w}")
            count = count +1  
            if count>=testlayers: break
        
        k_t, ApproGradsquare, ApproVar, ExpectGain = DynamicChoice(0, InfoList, t, args, num_workers, vDelay)
        if t>=2 and Losstoprint[-1]-Losstoprint[-2]>0.01*Losstoprint[-2] and k_t<dynamick[-1]:
            k_t = np.max([dynamick[-1], dynamick[-2]]) + 1
            if k_t>16 : k_t = 16
        vDelay.next(k_t)
 
        if t>0:
            delay_workers = vDelay.getPrevLateWorkerArrival()
            if len(delay_workers) >0:
                WithLateGrads,WithLateVar,num_recentworker,gradients_mean = GetWithLateInfo(workers_to_aggregate,delay_workers,gradients,gradients_to_add)
                Num_recentL.append(num_recentworker)
                InfoList[-1] = (t-1, num_recentworker, WithLateVar, WithLateGrads,copy.deepcopy(gradients_mean))
                #Here to avoid print issue
                _, WithLateGrads, WithLateVar,_ =DynamicChoice(0,InfoList,t,args,num_workers,vDelay)
            else: WithLateGrads,WithLateVar = ApproGradsquare, ApproVar
        else:  WithLateGrads, WithLateVar = None, None
        workers, workers_to_aggregate = check(RecvreqList, gradients, t, count_times, T,gradients_to_add, dynamick,vDelay,k_t)
        g_t_normsquare, var_t = Update(InfoList,workers_to_aggregate, gradients, gradients_to_add, t) 
        
        GradNormsquareL.append(ApproGradsquare)
        WithLateGradsL.append(g_t_normsquare)
        VarkL.append(ApproVar)
        WithLateVarkL.append(var_t)
        dynamick.append(k_t)
        ExpectedGainL.append(ExpectGain)
        if t > 1:
            timeprint.append(vDelay.getLastIterationTime())
       
        Loss = []
        for w in range(num_workers):
            lossw = gradients[w][0][-1]
            Loss.append(float( torch.reshape(lossw, shape=(-1,))[0].numpy() )) 
        Losstoprint.append(mean(Loss))

        if t%1 ==0:
            print("----------------------------------------------------------------------------------")
            print(f"Iter {t}: ps aggregates {k_t} gradients, Workers {workers_to_aggregate}, loss: {Losstoprint[-1]}, ")

        count = 0
        for layer in model.parameters():
            layer.grad = gradients_to_add[count]
            count = count + 1
            if count>=testlayers: break
        optimizer.step()
        t = t + 1
    filename = 'output_'+str(args.bsz)+'_'+str(args.lr)+'_'+str(num_workers)+'_'+data+'_'+args.distribution+'_'+str(args.times)
    print(filename)
    exists = os.path.isfile(filename)
    if exists: mode = 'ab'
    else: mode = 'wb'
    with open(filename,mode) as fp:
        pickle.dump((GradNormsquareL,VarkL, dynamick,timeprint,Losstoprint),fp)
 
    wait(RecvreqList, SendreqList,gradients, T)
    for w in range(num_workers):
        count = 0
        for layer in model.parameters():
            dist.send(layer.data,w)     
            count = count + 1
            if count>=testlayers : break        
    print("ps terminates")                   

def runworker(rank, size, args):
    torch.backends.cudnn.benckmark = False
    torch.backends.cudnn.deterministic = True

    """ Getting the parameters """
    lr = args.lr
    bsz = args.bsz
    epochnum = args.epochnum
    data = args.data
    device = args.device
    modelfix = args.modelfix
    nodes = args.nodes
    nproc_per_gpu = args.nproc_per_gpu

    T = args.T
    testlayers = args.layers


    
    """ Reading data """     
    torch.manual_seed(1234)
    train_set, test_set, bsz = read_data.getdata(data,bsz,rank,1) #replica data
    """ Building models """

    model = resnet.choosemodel(modelfix,data)

    """ Copying model to the device """

    if device == "gpu":
        if torch.cuda.is_available() == True:
            torch.cuda.set_device(int(rank%(size/nodes/nproc_per_gpu)))
            model = model.cuda()
        else :
            print("Exit: There is no gpu available for the test")
            exit()

    """ Choose Optimizer """  
    optimizer = optim.SGD(model.parameters(), lr=lr)

 
    Recvpv = []
    count = 0

    for param in model.parameters():
        Recvpv.append([])  
        count +=1
        if count>=testlayers: break

    step = 0
    outflag = False
    for epoch in range(epochnum):
        epoch_loss = 0.0
        for data, target in train_set:
            if device == "gpu":
                data, target = Variable(data.cuda()), Variable(target.cuda())
            else:      
                data, target = Variable(data), Variable(target)
     
            """Receiving from the PS"""

            if step == 0:
                count = 0
                for param in model.parameters():
                    Recvpv[count] = dist.irecv(param.data, size)
                    count = count + 1
                    if count>=testlayers: break
            count = 0
            for param in model.parameters():
                Recvpv[count].wait()
                Recvpv[count] = dist.irecv(param.data,size)    
                count = count +1    
                if count>=testlayers: break
            step = step + 1

            checkqueue = np.sum([req.is_completed() for req in Recvpv])
            while checkqueue==len(Recvpv):
                count = 0
                for param in model.parameters():                    
                    Recvpv[count] = dist.irecv(param.data,size)
                    count = count +1                        
                    if count>=testlayers: break
                step = step + 1
                checkqueue = np.sum([req.is_completed() for req in Recvpv])


            """ Calculate output, gradient """     
            
            optimizer.zero_grad()
            output = model(data)
            lossfunction = nn.CrossEntropyLoss()
            loss = lossfunction(output, target)            
            loss.backward()
 
            """Sending gradients to PS"""
            checkqueue = np.sum([req.is_completed() for req in Recvpv])
            if checkqueue == 0:
                for param in model.parameters():
                   # if True:#Recvpv[count].is_completed() == False: #if use this will bring the error of matching
            #        print(f"Iter {step-1}: worker {rank} sends new graidents of the {count}th layer")
                    param_to_send = param.grad.clone()
                    newsize = [1]
                    newsize.extend(list(param.grad.size()[1:]))
                    addt = torch.zeros(newsize).cuda()
                    addlayer = torch.zeros(newsize).cuda()
                    addt.fill_(step)
                    addlayer.fill_(loss)
                    param_to_send = torch.cat((param_to_send, addt, addlayer))
                    dist.send(param_to_send, size)
                    if count>=testlayers: break            
            optimizer.zero_grad()
            if step>T:
                outflag = True
                break

        if outflag == True:
            break

    for req in Recvpv:
        req.wait()   
    print(f"worker {rank} terminates")
 

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='PytorchMNIST')
    parser.add_argument('--lr', type = float, default = 0.001, metavar = 'N', 
                         help = 'learning rate (default 0.001)')
    parser.add_argument('--momentum', type = float, default = 0.0, metavar = 'N', 
                         help = 'momentum (default 0.0)')
    parser.add_argument('--decay', type = float, default = 0.0, metavar = 'N', 
                         help = 'decay (default 0.0)')
    parser.add_argument('--bsz', type = int, default = 100, metavar = 'N', 
                         help = 'batch size (default 100)')
    parser.add_argument('--epochnum', type = int, default = 10, metavar = 'N', 
                         help = 'epoch number (default 10)')
    parser.add_argument('--T', type = int, default = 10, metavar = 'N',
                         help = "number of iterations (default 10)")
    parser.add_argument('--k',type=int, default =1, metavar = 'N',
                         help = "number of useful gradients (default 1)")
    parser.add_argument('--data', type = str, default = "mnist", metavar = 'N', 
                         help = 'data (default mnist)')
    parser.add_argument('--device', type = str, default = "gpu", metavar = 'N', 
                         help = 'device (default gpu)')
    parser.add_argument('--modelfix', type = str, default = "simple", metavar = 'N', 
                         help = 'model (default convolutional network)')
    parser.add_argument('--nodes', type = int, default = 1, metavar = 'N', 
                         help = 'the number of cuda machines (default 1)')
    parser.add_argument('--layers',type= int, default = "10000", help=' the number of layers to transfer (default every layer)') 
    parser.add_argument('--nproc_per_gpu', type = int, default="1")
    parser.add_argument('--ngpu', type = int, default="1")
    parser.add_argument('--D', type = int, default = "1")
    parser.add_argument('--distribution', type = str, default = "realistic")
    parser.add_argument('--times', type=int, default="1")
    """ Getting parameters """
    args = parser.parse_args()
   
    """ Initialized the communication backend """
    dist.init_process_group(backend='mpi',world_size = args.nproc_per_gpu*args.ngpu+1)


    torch.cuda.empty_cache()
    size = dist.get_world_size()
    print(size)
    time_begin = time.time()
    rank = dist.get_rank()
    
    for index_t in range(args.times):
        if rank<size-1:
            print(f"worker:{rank} starts")
            runworker(rank,size-1,args)
        else:
            print(f"ps:{rank} starts")
            runps(args.nproc_per_gpu*args.ngpu,args, index_t)
    time_used = time.time()-time_begin
    print("Total execution time = {:.3f}s".format(time_used))
