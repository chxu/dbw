import csv
import numpy as np
import torch
import torch.distributed as dist
import torch.nn as nn
from torchvision import datasets, transforms
from random import Random
class Partition(object):
    """ Dataset-like object, but only access a subset of it. """

    def __init__(self, data, index):
        self.data = data
        self.index = index

    def __len__(self):
        return len(self.index)

    def __getitem__(self, index):
        data_idx = self.index[index]
        return self.data[data_idx]

class DataPartitioner(object):
    """ Partitions a dataset into different chuncks. """

    def __init__(self, data, sizes=[0.7, 0.2, 0.1], seed=1234):
        self.data = data
        self.partitions = []
        rng = Random()
        rng.seed(seed)
        data_len = len(data)
        indexes = [x for x in range(0, data_len)]
        rng.shuffle(indexes)
      #  print(indexes[:10])
        for frac in sizes:
            part_len = int(frac * data_len)
            self.partitions.append(indexes[0:part_len])
            indexes = indexes[part_len:]

    def use(self, partition):
        return Partition(self.data, self.partitions[partition])


def Load_MNIST(batchsize,rank,size):
   
    bsz = batchsize
    """ Partitioning MNIST """
    dataset = datasets.MNIST(
        './data/mnist',
        train=True,
        download=False,
        transform=transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize((0.1307, ), (0.3081, ))
        ]))
    testset = torch.utils.data.DataLoader(
        datasets.MNIST('./data/mnist',train=False, transform=transforms.Compose([
                           transforms.ToTensor(),
                           transforms.Normalize((0.1307,),(0.3081,))
                      ])),
        batch_size = bsz, shuffle = True)
    partition_sizes = [1.0 / size for _ in range(size)]
    partition = DataPartitioner(dataset, partition_sizes,seed=rank)
    partition = partition.use(0) #copy for every one
    train_set = torch.utils.data.DataLoader(
        partition, batch_size=bsz, shuffle=False)
    return train_set, testset, bsz




def getdata(data,batchsize,rank,size):
    if data == "mnist":
        return Load_MNIST(batchsize,rank,size)
    else: 
        print(f"Error: No data: {data}")
        exit(0)
















