from VirtualDelay import VirtualDelay
import numpy as np
import copy
import matplotlib.pyplot as plt

"""

Idea: Generate virtual workers computation time for the the "worker reset" model.
The fact that the model is "worker reset" means that once started a gradient computation the workers can be stopped if
the PS has started a new iteration so there is not the problem of considering 

"""



class VirtualDelayPS(VirtualDelay):

    def __init__(self, epoch, workers, distribution, estimator, test,num_tries):
        super().__init__(epoch, workers, distribution, estimator, test,num_tries)
        if self.test:
            self.ax.set_title('Push & Interrupt - Workers workflow')


    # Compute the delays for the next iteration using a given k
    def next(self, k):
        self.k.append(k)
        self.vPrev = copy.deepcopy(self.vCurr)
        self.vCurr = self.vGen[self.i]+self.iterationTime[self.i]
        vCurrSorted = copy.deepcopy(self.vCurr)
        vCurrSorted.sort(axis=0)  # Sorted current time
        # saving ending time of the iteration
        self.iterationTime = np.append(self.iterationTime, vCurrSorted[k-1] + self.PSCompTIme)
        j = 0

        # saving valid arrival time of the current iteration
        for elem in vCurrSorted:
            if elem <= self.iterationTime[self.i + 1]:
                idx = np.where(self.vCurr == elem)
                self.arrivalTime[j].append(elem)  # adding to the correct position the new arrival
                self.delay[j].append(elem-self.iterationTime[self.i])  # saving computation delay for a specific k
                self.nDelaysK[j] += 1  # Updating the total number of received delays for a give k
                self.nDelays += 1  # Updating the total number of received delays
                self.nUsefulWorker[idx] += 1  # Updating the number of valid processed gradients for a specific worker
                j += 1
            self.nProcessedWorker[idx] += 1  # Updating the number of processed gradients for a specific worker
        self.computeTau()
        self.i += 1  # update current iteration

        # if in test mode print details about the execution and track the workflow of the workers
        if self.test:
            print("========== Iteration", self.i - 1, "==========")
            print("vCurr", self.vCurr)
            print("CurrWorkerArrival", self.getCurrWorkerArrival())
            print("vGen", self.vGen[self.i-1])
            print("arrival time", self.arrivalTime)
            print("delay", self.delay)
            print("tau", self.getLastTau())
            print("number arrived", self.nDelaysK)
            print("total delays", self.nDelays)
            print("start iteration", self.iterationTime[self.i-1])
            print("finish iteration", self.iterationTime[self.i])
            self.trackWorkflow(self.ax)

    # has to be run at each iteration, it will create barplots for every worker considering the starting and final
    # time of a computation and also if for that specific computation the worker is a struggler or not

    def trackWorkflow(self, ax):
        finVal = (self.vGen[self.i - 1])[((self.iterationTime[self.i-1] + self.vGen[self.i - 1]) <= self.iterationTime[self.i])]
        initVal = (self.iterationTime[self.i - 1])
        workers = np.arange(1, self.workers + 1, 1)[((self.iterationTime[self.i-1] + self.vGen[self.i - 1]) <= self.iterationTime[self.i])]
        bar = ax.bar(workers, finVal, edgecolor='black', bottom=initVal, color='seagreen')
        for idx, rect in enumerate(bar):
            plt.text(rect.get_x() + rect.get_width() / 2.0, finVal[idx] + initVal,
                     '%i' % (self.i-1), ha='center', va='bottom')

        initVal = (self.iterationTime[self.i - 1])
        workers = np.arange(1, self.workers + 1, 1)[((self.iterationTime[self.i - 1] + self.vGen[self.i - 1]) > self.iterationTime[self.i])]
        ax.bar(workers, self.iterationTime[self.i] - self.iterationTime[self.i-1], edgecolor='black', bottom=initVal, color='indianred')
        ax.axhline(y=self.iterationTime[self.i], color='b', linestyle=':')

