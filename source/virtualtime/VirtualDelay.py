import numpy as np
import os
import matplotlib.pyplot as plt
from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures
from sklearn.pipeline import make_pipeline
from matplotlib.patches import Patch


class VirtualDelay:

    def __init__(self, epoch, workers, distribution, estimator, test, num_tries):
        self.num_tries = num_tries
        self.workers = workers  # Number of workers
        self.i = 0  # Current iteration
        self.epoch = epoch  # Total number of iterations
        self.k = []  # k for the current iteration
        self.vPrev = []  # Virtual time of the previous iteration
        self.vCurr = np.zeros(self.workers)  # Virtual time of workers for the current iteration
        self.vGen = self.genvTimes(epoch, workers,
                                   distribution)  # Generated virtual time of the workers for each iteration
        self.delay = [[] for _ in range(self.workers)]  # All delays for each k at each iteration
        self.arrivalTime = [[] for _ in range(self.workers)]  # All arrival times for each k at each iteration
        self.iterationTime = np.array([0.])  # Starting time of each iteration
        self.nDelays = 0    # total number of times generated till now
        self.nDelaysK = np.zeros(self.workers)  # total number of times generated till now per k
        self.nProcessedWorker = np.zeros((self.workers,), dtype=int)  # Number of time a worker has processed a gradient
        self.nUsefulWorker = np.zeros((self.workers,), dtype=int)  # Number of time a worker has processed a valid gradient
        self.tau = [[] for _ in range(self.workers)]  # Structure that contain the estimation of tau
        self.estimator = estimator  # Type of used estimator
        self.test = test  # Specify if is in test mode
        self.PSCompTIme = 0.0
        if self.test:
            fig, self.ax = plt.subplots()

    # Times generator from a given distributions, the time are generated if a configuration doesn't already exists or
    # loaded if the configuration already exists
    def genvTimes(self, epoch, workers, distribution):
        if distribution == "normal":
            path = "distributions/normal_" + str(epoch) + "_" + str(workers) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = np.random.normal(50, 1, (epoch, workers))
                np.savetxt(path, d)

        elif distribution == "uniform":
            path = "distributions/uniform_" + str(epoch) + "_" + str(workers) + "str(self.num_tries)" + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = np.random.uniform(0, 0.5, (epoch, workers))
                np.savetxt(path, d)

        elif distribution == "exponential":
            path = "distributions/exponential_" + str(epoch) + "_" + str(workers) + "_" + str(self.num_tries) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = 1+0*np.random.exponential(1, (epoch, workers))
                np.savetxt(path, d)
        elif distribution == "exponential1":
            path = "distributions/exponential1_" + str(epoch) + "_" + str(workers) + "_" + str(self.num_tries) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = 0.5+0.5*np.random.exponential(1, (epoch, workers))
                np.savetxt(path, d)
        elif distribution == "exponential2":
            path = "distributions/exponential2_" + str(epoch) + "_" + str(workers) + "_" + str(self.num_tries) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = 0+1*np.random.exponential(1, (epoch, workers))
                np.savetxt(path, d)
        elif distribution == "exponential3":
            path = "distributions/exponential3_" + str(epoch) + "_" + str(workers) + "_" + str(self.num_tries) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = 0.3+0.7*np.random.exponential(1, (epoch, workers))
                np.savetxt(path, d)
        elif distribution == "exponential4":
            path = "distributions/exponential4_" + str(epoch) + "_" + str(workers) + "_" + str(self.num_tries) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = 0.8+0.2*np.random.exponential(1, (epoch, workers))
                np.savetxt(path, d)






        elif distribution == "pareto":
            path = "distributions/pareto_" + str(epoch) + "_" + str(workers) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = np.random.pareto(0.8, (epoch, workers))
                np.savetxt(path, d)

        elif distribution == "pareto1":
            path = "distributions/pareto1_" + str(epoch) + "_" + str(workers) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                d = 0.5+0.5*np.random.pareto(1, (epoch, workers))
                np.savetxt(path, d)



        elif distribution == "bernoulli1":  #  (90% 1s, 10% 10s)
            path = "distributions/bernoulli1_" + str(epoch) + "_" + str(workers)  + "_" + str(self.num_tries) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                noise = np.random.normal(0, 1.0e-8, (epoch, workers))
                d = np.random.choice([1, 10], (epoch, workers), p=[0.9375, 0.0625]) + noise
                np.savetxt(path, d)

        elif distribution == "bernoulli2":  #  (75% 1s, 25% 10s)
            path = "distributions/bernoulli2_" + str(epoch) + "_" + str(workers) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                noise = np.random.normal(0, 1.0e-8, (epoch, workers))
                d = np.random.choice([1, 10], (epoch, workers), p=[0.75, 0.25]) + noise
                np.savetxt(path, d)

        elif distribution == "bernoulli3":  #  (50% 1s, 50% 10s)
            path = "distributions/bernoulli3_" + str(epoch) + "_" + str(workers) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                noise = np.random.normal(0, 1.0e-8, (epoch, workers))
                d = np.random.choice([1, 10], (epoch, workers), p=[0.5, 0.5]) + noise
                np.savetxt(path, d)

        elif distribution == "constant":
            path = "distributions/constant_" + str(epoch) + "_" + str(workers) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                noise = np.random.normal(0, 0.000001, (epoch, workers))
                d = np.full((epoch, workers), 5) + noise
                np.savetxt(path, d)
        elif distribution == "realistic":
            path = "distributions/realistic_" + str(epoch) + "_" + str(workers) + "_" + str(self.num_tries) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                pathGen = "distributions/spark_sgd_timings.txt"
                exists = os.path.isfile(pathGen)
                if exists:
                    gen = np.loadtxt(pathGen)
                    d = np.random.choice(gen, (epoch, workers)) / 1000
                    np.savetxt(path, d)
                else:
                    raise Exception("File spark_sgd_timings.txt not found")
        elif distribution == "decreasing":
            path = "distributions/decreasing_" + str(epoch) + "_" + str(workers) + "_" + str(self.num_tries) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                slowIter=20
                noise1 = np.random.normal(0, 0.001, (epoch - slowIter, workers))
                noise2 = np.random.normal(0, 0.001, (slowIter, 7))
                noise3 = np.random.normal(0, 0.001, (slowIter, 9))
                d = np.append(np.append(np.full((slowIter, 7), 10) + noise2,
                              np.full((slowIter, 9), 3) + noise3
                              , axis=1), np.full((epoch - slowIter, workers), 3) + noise1, axis=0)
                np.savetxt(path, d)
        elif distribution == "increasing":
            path = "distributions/increasing" + str(epoch) + "_" + str(workers) + ".txt"
            exists = os.path.isfile(path)
            if exists:
                d = np.loadtxt(path)
            else:
                slowIter = 31
                middle = epoch-slowIter
                noise1 = np.random.normal(0, 0.001, (slowIter, workers))
                noise2 = np.random.normal(0, 0.001, (epoch-slowIter, int(workers/2)))
                d = np.append(np.full((slowIter, workers), 5),
                              np.append(np.full((middle, 8), 5),
                              np.full((middle, 8),25) 
                              , axis=1), axis=0)
               # d = np.append(d,np.full((epoch-slowIter-middle,workers),5),axis = 0)
                np.savetxt(path, d)

        else:
            raise Exception("Given distribution not found")

        return d

    # Compute the delays for the next iteration using a given k
    def next(self, k):
        pass

    # Internal function that run a selected estimator of tau
    def computeTau(self):
        if self.estimator == 'mean':
            self.meanTau()
        elif self.estimator == 'regression':
            self.regressionTau()
        else:
            raise Exception('The selected estimator %s is not implemented yet, please select one between [mean, regression]' % self.estimator)

    # Mean estimator of tau for each k
    def meanTau(self):
        for k in range(self.workers):
            if len(self.delay[k]) == 0:
                self.tau[k].append(float('nan'))
            elif k != 0:
                self.tau[k].append(max(self.tau[k - 1][self.i], sum(self.delay[k]) / len(self.delay[k])))
            else:
                self.tau[k].append(sum(self.delay[k]) / len(self.delay[k]))

    # Regression estimator of tau for each k
    def regressionTau(self):
        ds = [[x for b in [[[i + 1] for _ in range(len(self.delay[i]))] for i in range(len(self.delay))] for x in b],
              [x for b in
               [x for b in [[[[self.delay[i][j]]] for j in range(len(self.delay[i]))] for i in range(len(self.delay))]
                for x in b] for x in b]]
        model = make_pipeline(PolynomialFeatures(3), Ridge())
        model.fit(ds[0], ds[1])
        pred = model.predict([[i + 1] for i in range(self.workers)]).squeeze().tolist()
        for k in range(len(pred)):
            if k != 0:
                self.tau[k].append(max(self.tau[k-1][self.i], pred[k]))
            else:
                self.tau[k].append(pred[k])

    # return the entire delay list
    def getDelay(self):
        return self.delay

    # return the entire arrival time list
    def getArrivalTime(self):
        return self.arrivalTime

    # return the number of total computed delays
    def getTotalDelaysNumber(self):
        return self.nDelays

    # return the list containing the number of computed delays for every k
    def getDelayNumber(self):
        return self.nDelaysK

    # return the number of computed delay for a given k
    def getDelayNumberK(self, k):
        return self.nDelaysK[k - 1]

    # return the finish computation time of the current iteration
    def getLastIterationTime(self):
        return self.iterationTime[-1]

    # return generated time for the current iteration
    def getGeneratedTime(self):
        return self.vGen[self.i-1]

    # return the ordered id of arrived workers for the current iteration
    def getCurrWorkerArrival(self):
        return np.argsort(self.vCurr)[:self.k[-1]].tolist()

    # return the last estimation of tau for a specific k
    def getLastTauK(self, k):
        return np.array(self.tau)[k-1, self.i-1]

    # return the array of estimation of tau for every k
    def getLastTau(self):
        return np.array(self.tau)[:, self.i-1]

    # return all the tau of every iteration for a specific k
    def getTauK(self, k):
        return np.array(self.tau)[k-1]

    # tack the workflow of the workers, needed for the plot
    def trackWorkflow(self, ax):
        pass

    # plot the workflow of the workers in a bar form specifying also if it is a struggler or not
    def plotWorkflow(self):

        self.ax.set_yticks(self.iterationTime)
        legend_elements = [Patch(facecolor='seagreen', edgecolor='black', label='Valid'),
                           Patch(facecolor='indianred', edgecolor='black', label='Strugglers'), ]
        self.ax.legend(handles=legend_elements)
        self.ax.set_xlabel('Workers')
        self.ax.set_ylabel('Time')
        self.ax.grid()
        lim1 = self.ax.get_ylim()
        ax2 = self.ax.twinx()
        ax2.set_ylim(lim1)
        ax2.set_yticks(self.iterationTime)
        labels=[]
        for i in range(self.epoch):
            labels.append('%i, k=%i' % (i, self.k[i]))
        ax2.set_yticklabels(labels)
        ax2.set_ylabel('Iterations')

    # plot of the estimated tau for every k
    def plotTime(self):
        fig, ax = plt.subplots()
        tau = self.getLastTau()
        # criteria = [0.18261512, 0.35321117, 0.51662515, 0.67712737, 0.83797233, 1.00274776, 1.17419868, 1.35578124,
        #            1.55121071, 1.76658085, 2.00863314, 2.28890927, 2.62753342, 3.0647402, 3.69615592, 4.90145975]
        plt.plot(np.arange(1, self.workers + 1, 1), tau, label='Estimation')
        #plt.plot(np.arange(1, self.workers + 1, 1), criteria, label='Criteria')
        plt.xlabel('K')
        plt.ylabel(r'$\tau_{k,end}$')
        plt.title('Time estimation')
        plt.legend()
        ax.grid()

        fig, ax = plt.subplots()
        for k in range(1, self.workers + 1):
            plt.plot(np.arange(1, self.epoch + 1, 1), self.getTauK(k), label='k=%i' % k)
        plt.xlabel('Iterations')
        plt.ylabel(r'$\tau_{k,t}$')
        plt.title('Time estimation')
        plt.legend()
        ax.grid()

    # plot every graph
    def plot(self):
        if self.test:
            self.plotWorkflow()
        self.plotTime()




