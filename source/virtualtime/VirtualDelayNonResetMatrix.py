import copy
from source.virtualtime.VirtualDelay import VirtualDelay
import numpy as np
import matplotlib.pyplot as plt
from source.virtualtime import TimeEstimator
"""

Idea: Generate virtual workers computation time for the NON-"worker reset" model.
The fact that the model is NON-"worker reset" means that once started a gradient computation the workers will finish it 
and will send the gradient to the PS even if is an old one.
This mean that we have to consider many factors that make the generation over time complex. 
The factors that influence the algorithm are the selected k, that can let the PS wait for every or just one 
gradient before starting a new iteration introducing so an idle time of for the workers that finished their gradient
computation, the fact that at the end of each iteration we want to see only the delays that arrived just up to that
moment and not also the future ones, the fact that some workers can be many iterations late. 
To face those factors is important to take trace of the previous and also analyse the future arrival time to
obtain the correct arrival values and so delay for the current iteration.

"""


class VirtualDelayPS(VirtualDelay):

    def __init__(self, epoch, workers, distribution, estimator, test,num_tries):
        super().__init__(epoch, workers, distribution, estimator, test,num_tries)
        if self.test:
            self.ax.set_title('Non-Reset Model - Workers workflow')

        self.workersIteration = np.zeros(self.workers, dtype=np.int)
        self.initWorkersIteration = np.zeros(self.workers, dtype=np.int)
        self.nIter = np.zeros(self.epoch, dtype=np.int)
        self.sel = []
        self.latePrev = []
        self.n = np.zeros([self.workers, self.workers])
        self.delayMean = np.zeros([self.workers, self.workers])
        self.tauMatrix = np.zeros([self.workers, self.workers])
    # Compute the delays for the next iteration using a given k
    def next(self, k):
        self.k.append(k)
        self.latePrev = []
        idleTime = (self.iterationTime[self.i] - self.vCurr).clip(
            min=0)  # Compute the time a worker has been idle from the previous iteration
        # compute the future virtual time in order to predict if in the next iteration a worker will still
        # be working and readapt so the current computation time.
        vFut = self.vCurr + idleTime + self.vGen[self.i]
        vFutSorted = copy.deepcopy(vFut)  # sorting the future virtual time
        vFutSorted.sort(axis=0)
        # Generating the selector that will exclude workers that will be still computing on the next iteration

        self.sel = (self.vCurr - vFutSorted[k - 1] < 0).astype(int)
        self.vPrev = copy.deepcopy(self.vCurr)  # Save current time as previous for the next iteration
        # Update the current time as current time + idle time + (generated time * selector)
        self.vCurr += idleTime + self.vGen[self.i] * self.sel
        # Save the starting time of the next iteration
        self.iterationTime = np.append(self.iterationTime, vFutSorted[k - 1] + self.PSCompTIme)

        # Arrival times computation & delays


        # save workers that will arrive late just from the previous iteration
        self.latePrev, = np.where((self.vPrev > self.iterationTime[self.i]) &
                                  (self.vPrev <= self.iterationTime[self.i + 1]))
        self.latePrev = self.latePrev[(self.workersIteration[self.latePrev] == self.i-1)]

        # Add remaining valid computation time & delays of the previous iteration
        for idx in np.argsort(self.vPrev):
            if (self.vPrev[idx] > self.iterationTime[self.i]) & (self.vPrev[idx] <= self.iterationTime[self.i + 1]): # check if the previous virtual time is in the correct time window
                # adding to the correct position the new arrival time based on the iteration of the worker that computed
                # it and to the number of times already evaluated for that iteration
                self.arrivalTime[self.nIter[self.workersIteration[idx]]].append(self.vPrev[idx])
                # computing the delay based on the end time and the starting time of the iteration that
                # the specific worker  was computing
                d = self.vPrev[idx] - self.iterationTime[self.workersIteration[idx]]
                # adding to the correct position the new delay based on the iteration of the worker that computed
                # it and to the number of times already evaluated for that iteration
                self.delay[self.nIter[self.workersIteration[idx]]].append(d)
                # Updating the number of delays that a specific k has received
                self.nDelaysK[self.nIter[self.workersIteration[idx]]] += 1
                self.n[self.k[self.workersIteration[idx]-1]-1, self.nIter[self.workersIteration[idx]]] += 1


                if self.nIter[self.workersIteration[idx]] != 0:
                    self.delayMean[
                        self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]] = max(
                        self.delayMean[
                            self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]] - 1],
                        self.delayMean[
                            self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]] + (
                                d - self.delayMean[
                            self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]]) /
                        self.n[self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]])
                else:
                    self.delayMean[
                        self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]] += \
                        (d - self.delayMean[self.k[self.workersIteration[idx] - 1] - 1, self.nIter[
                            self.workersIteration[idx]]]) / self.n[self.k[self.workersIteration[idx] - 1] - 1,
                                                                   self.nIter[self.workersIteration[idx]]]


                # Updating the number of received time in a specific iteration
                self.nIter[self.workersIteration[idx]] += 1
                # Updating the current iteration of the worker
                self.workersIteration[idx] = self.i
                # Updating the total number of received delays
                self.nDelays += 1
                # Updating the number of processed gradients for a specific worker
                self.nProcessedWorker[idx] += 1

        # Add valid computation time & delay of the current iteration
        for idx in np.argsort(self.vCurr):
            if self.vCurr[idx] <= self.iterationTime[self.i + 1]:
                self.arrivalTime[self.nIter[self.workersIteration[idx]]].append(self.vCurr[idx])
                d = self.vCurr[idx] - self.iterationTime[self.workersIteration[idx]]
                self.delay[self.nIter[self.workersIteration[idx]]].append(d)
                self.nDelaysK[self.nIter[self.workersIteration[idx]]] += 1
                self.n[self.k[self.workersIteration[idx] - 1] -1, self.nIter[self.workersIteration[idx]]] += 1

                if self.nIter[self.workersIteration[idx]] != 0:
                    self.delayMean[
                        self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]] = max(
                        self.delayMean[
                            self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]-1],
                        self.delayMean[
                            self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]] + (
                                d - self.delayMean[
                            self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]]) /
                        self.n[self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]])
                else:
                    self.delayMean[
                        self.k[self.workersIteration[idx] - 1] - 1, self.nIter[self.workersIteration[idx]]] += \
                        (d - self.delayMean[self.k[self.workersIteration[idx] - 1] - 1, self.nIter[
                            self.workersIteration[idx]]]) / self.n[self.k[self.workersIteration[idx] - 1] - 1,
                                                                   self.nIter[self.workersIteration[idx]]]

                self.nIter[self.workersIteration[idx]] += 1
                self.workersIteration[idx] = self.i + 1
                self.nDelays += 1
                # Updating the number of valid processed gradients for a specific worker
                self.nUsefulWorker[idx] += 1
                self.nProcessedWorker[idx] += 1

        self.computeTau()
        self.i += 1  # update current iteration

        # if in test mode print details about the execution and track the workflow of the workers
        if self.test:
            print()
            print("========== Iteration", self.i-1, "K=", self.k[-1], "==========")
            print("vCurr", self.vCurr)
            print("CurrWorkerArrival", self.getCurrWorkerArrival())
            print("PrevLateWorkerArrival", self.getPrevLateWorkerArrival())
            print("Sel", self.sel)
            print("vGen", self.vGen[self.i-1])
            print("Arrival time", self.arrivalTime)
            print("Delay", self.delay)
            print("N", self.n)
            print("Delay mean", self.delayMean)
            print("Last Tau", self.getLastTau())
            print("Number arrived", self.nDelaysK)
            print("Total delays", self.nDelays)
            print("Workers Iteration", self.workersIteration)
            print("Start iteration", self.iterationTime[self.i-1])
            print("Finish iteration", self.iterationTime[self.i])
            self.trackWorkflow(self.ax)

    def computeTau(self):
        self.tauMatrix = TimeEstimator.Solver_time(copy.deepcopy(self.delayMean), self.n)
        for k in range(self.workers):
            self.tau[k].append(self.tauMatrix[self.k[-2 if self.i != 0 else -1]-1, k])
        return self.tauMatrix
        """
        tau = copy.deepcopy(self.delayMean)
        for i in range(self.workers-1, -1, -1):
            for j in range(self.workers):
                if j != 0:
                    tau[i, j] = max(tau[i, j-1], tau[i, j])
                else:
                    tau[i, j] = tau[i, j]
                if i != 0:
                    tau[i-1, j] = max(tau[i-1, j], tau[i, j])
        for k in range(self.workers):
            self.tau[k].append(tau[self.k[-2 if self.i != 0 else -1]-1, k])
        """
       # return tau

    # return the ordered id of late worker for the current iteration
    def getPrevLateWorkerArrival(self):
        return self.latePrev[np.argsort(self.vPrev[self.latePrev])].tolist()

    # has to be run at each iteration, it will create barplots for every worker considering the starting and final
    # time of a computation and also if for that specific computation the worker is a struggler or not
    def trackWorkflow(self, ax):
        self.sel = np.array(self.sel)
        finVal = (self.vGen[self.i - 1])[(self.sel != 0) & (self.vPrev > self.iterationTime[self.i - 1])]
        initVal = (self.vPrev[(self.sel != 0) & (self.vPrev > self.iterationTime[self.i - 1])])
        workers = np.arange(1, self.workers + 1, 1)[(self.sel != 0) & (self.vPrev > self.iterationTime[self.i - 1])]
        bar = ax.bar(workers, finVal, edgecolor='black', bottom=initVal)
        for idx, rect in enumerate(bar):
            if self.vCurr[workers[idx] - 1] <= self.iterationTime[self.i]:
                rect.set_color('seagreen')
                rect.set_edgecolor('black')
                plt.text(rect.get_x() + rect.get_width() / 2.0, finVal[idx] + initVal[idx],
                         '%i' % (self.workersIteration[workers[idx] - 1] - 1), ha='center', va='bottom')
            else:
                rect.set_color('indianred')
                rect.set_edgecolor('black')
                plt.text(rect.get_x() + rect.get_width() / 2.0, finVal[idx] + initVal[idx],
                         '%i' % self.workersIteration[workers[idx] - 1], ha='center', va='bottom')

        finVal = (self.vGen[self.i - 1])[(self.sel != 0) & (self.vPrev <= self.iterationTime[self.i - 1])]
        initVal = (self.iterationTime[self.i - 1])
        workers = np.arange(1, self.workers + 1, 1)[(self.sel != 0) & (self.vPrev <= self.iterationTime[self.i - 1])]
        # workersStrugg = np.arange(1, self.workers + 1, 1)[(self.sel != 0) & (self.vCurr > self.iterationTime[self.i])]
        bar = ax.bar(workers, finVal, edgecolor='black', bottom=initVal)
        for idx, rect in enumerate(bar):
            if self.vCurr[workers[idx] - 1] <= self.iterationTime[self.i]:
                rect.set_color('seagreen')
                rect.set_edgecolor('black')
            else:
                rect.set_color('indianred')
                rect.set_edgecolor('black')
            plt.text(rect.get_x() + rect.get_width() / 2.0, finVal[idx] + initVal,
                     '%i' % self.initWorkersIteration[workers[idx] - 1], ha='center', va='bottom')

        ax.axhline(y=self.iterationTime[self.i], color='b', linestyle=':')
        self.initWorkersIteration = copy.deepcopy(self.workersIteration)

    def plotTime(self):
        fig, ax = plt.subplots()
        tau = self.computeTau()
        # criteria = [0.18261512, 0.35321117, 0.51662515, 0.67712737, 0.83797233, 1.00274776, 1.17419868, 1.35578124,
        #            1.55121071, 1.76658085, 2.00863314, 2.28890927, 2.62753342, 3.0647402, 3.69615592, 4.90145975]
        for i in range(self.workers):
            plt.plot(np.arange(1, self.workers + 1, 1), tau[i], label='Estimation Kold=%i' % (i + 1))
        # plt.plot(np.arange(1, self.workers + 1, 1), criteria, label='Criteria')
        plt.xlabel('K')
        plt.ylabel(r'$\tau_{k,end}$')
        plt.title('Time estimation')
        plt.legend()
        ax.grid()

        fig, ax = plt.subplots()
        for k in range(1, self.workers + 1):
            plt.plot(np.arange(0, self.epoch + 1, 1), self.getTauK(k), label='k = %i' % k)
        plt.xlabel('Iterations')
        plt.ylabel(r'$\tau_{k,t}$')
        plt.title('Time estimation')
        plt.legend()
        ax.grid()
