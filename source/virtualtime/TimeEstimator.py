import numpy as np
from cvxopt import matrix
from cvxopt import solvers
solvers.options['show_progress'] = False

def Solver_time(Tbar, N):
    n = Tbar.shape[0]
    G = np.zeros((2*n*(n-1)+(n-1),n**2))
    h = np.zeros(2*n*(n-1))
    for i in range(0,n*(n-1)):
        gx = i
        gy = int(i/(n-1))*n+i%(n-1)
        G[gx,gy] = 1
        G[gx,gy+1] = -1
    for i in range(n*(n-1), 2*n*(n-1)):
        gy = i-(n-1)*n
        G[i, gy] = -1
        G[i, gy+n] = 1
    for i in range(2*n*(n-1), 2*n*(n-1)+(n-1)):
        gy = (i-2*n*(n-1))*(n+1)
        G[i, gy] = 1
        G[i, gy+ (n+1)] = -1
    P = np.eye(n**2)*(N.reshape(n**2)*2)*np.ones(n**2)
    q = -2*Tbar.reshape(n**2)*N.reshape(n**2)
    #model:
    #min 1/2x^tPx + q^tx
    #st Gx<=h
    """
    P = Diag(2*N.reshape(n**2))
    """
    P = matrix(P, tc='d')
    G = matrix(G, tc='d')
    h = matrix(np.zeros(n*(n-1)*2+(n-1)), tc='d')
    q = matrix(q, tc='d')
    sol = solvers.qp(P, q, G, h)
    solution = np.array(sol['x']).reshape(n, n)
    """
    print(f"Emperical T:\n{Tbar}")
    print(f"Emperical N:\n{N}")
    print(f"solution T*:\n{solution}")
    """
    return solution



"""
n=4
Tbar = np.random.rand(n,n)*10 #Emperical Time Matrix
N = np.random.randint(20, size =(n,n)) #Emperical N Matrix
Tbar = Solver_time(Tbar, N)
"""
