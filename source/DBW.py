import torch
import torch.distributed as dist
import numpy as np
import copy
def wait(list1, list2,gradients, T):
    num_layers = len(list1)
    num_workers =len(list1[0])

    for layer in range(num_layers):
        for worker in range(num_workers):
    #        print(f"ps waiting for worker {worker} layer {layer}")
            list1[layer][worker].wait()
   #         print(f"ps continues to check")
            data_iter = gradients[worker][layer][-2]
            iter = int( torch.reshape(data_iter, shape=(-1,))[0].numpy() )     
            if iter<T+1:
                 dist.recv(gradients[worker][layer],worker)
               #  data_iter = gradients[worker][layer][-2]
               #  iter = int( torch.reshape(data_iter, shape=(-1,))[0].numpy() )     
    print("Receiving request done")
    for req in list2:
        req.wait()

    print("Sending request done")

def check(RecvreqList, gradients, t, count_times, T,gradients_to_add, dynamick,vDelay,k):
    workers = []
  #  print(len(RecvreqList),len(RecvreqList[0]))
    num_layers = len(RecvreqList)
    num_workers = len(RecvreqList[0])
    for layer in range(num_layers):
        workers.append([])
        gradients_to_add[layer].zero_()
    Jump = True
    Copydynamick = dynamick
    workers_to_aggregate = vDelay.getCurrWorkerArrival()
   # print(f"k:{k} and number of workers:{len(workers_to_aggregate)}") 
    while Jump:
        for w in range(num_workers):
            for layer in range(num_layers):
                if RecvreqList[layer][w].is_completed():                 
                    data_iter = gradients[w][layer][-2]
                    iter = int( torch.reshape(data_iter, shape=(-1,))[0].numpy() )   
                    if layer ==0 : workerfix = [i for i in range(num_workers)]
                    else: workerfix = workers[layer-1]                 
                    if w not in workers[layer] and iter == t+1 and len(workers[layer])<num_workers and w in workerfix:
                        workers[layer].append(w)
                        if w in workers_to_aggregate:
                            count_times[w][layer] = count_times[w][layer] + 1
                            gradients_to_add[layer].add_(1/k,gradients[w][layer][:-2])
                           # print(f"Iter {t}: ps aggregates the {layer}th layer from worker {w}, with k = {k} and workers = {workers_to_aggregate}")
                        if t<T:
                            RecvreqList[layer][w] = dist.irecv(gradients[w][layer],w)
                        if np.sum([len(workers[l])==num_workers for l in range(num_layers)]) == num_layers:
                            Jump = False
                            break
                    if iter<t+1 and t< T:
                        RecvreqList[layer][w] = dist.irecv(gradients[w][layer],w)
                if Jump == False: break
            if Jump == False: break
    return workers, workers_to_aggregate
    
def DynamicChoice( L, InfoList, t, args, num_workers, vDelay):
    if t == 0:
        k_t = args.k
        ExpectGain = None
        ApproGradsquareMean = None
        ApproVarMean = None
        return k_t, None, None, None 
    else: 
        min_t = np.max([t-args.D, 0])
        max_t = t-1

        ApproVar = 0.0
        ApproGradsquare = 0.0
        index = [i for i in range(min_t,max_t+1)]
       # print(index) 
        count = 0
        gradients_sum = InfoList[0][4].copy()
        for index_t in range(min_t, max_t+1):
            for grad_layer in gradients_sum:
                grad_layer.zero_()
 
            time, k, Var, GradNormsquare, gradients_mean= InfoList[index_t]
            if k>1: 
                ApproVar += Var/(k-1) 
                toaddgrad = GradNormsquare - Var/(k-1)/k
            else: 
                ApproVar += Var
                toaddgrad = GradNormsquare
            if toaddgrad > 0:
                ApproGradsquare += toaddgrad
        ApproGradsquareMean = ApproGradsquare / len(index)     
        ApproVarMean = ApproVar / len(index) 
        ExpectGain = [ApproGradsquareMean - ApproVarMean/k for k in range(1,num_workers+1)]               
        delay = vDelay.getDelay()
        Tau = [sum(delayk)/len(delayk) for delayk in delay]
        print(f"Tau : {Tau}")
        k_list = [k for k in range(1, num_workers+1)]
        gainovertime =  [gain/tau for gain,tau in zip(ExpectGain, Tau)]
        print(f"Gain over time {gainovertime}")
        k_t = np.argmax(gainovertime) + 1    
        print(f"Choose k= {k_t}") 
    return k_t,ApproGradsquareMean, ApproVarMean, ExpectGain[k_t-1]

def GetWithLateInfo(workers,lateworkers,gradients,gradients_to_add):
    recentworkers = workers + lateworkers
    print(f"Lateworkers:{lateworkers},NonLateworkers:{workers}")
    num_recentworker = len(recentworkers)
    gradients_mean = gradients_to_add.copy()
    for grad_layers in gradients_mean:
        grad_layers.zero_() 
    WithLateVar = 0
    WithLateGradNormsquare = 0
    for w in recentworkers:
        g_it = gradients[w]
        for layer_g_git,layer_g_mean in zip(g_it,gradients_mean):
            layer_g_it_trun = layer_g_git[:-2]
            layer_g_mean.add_(1/num_recentworker,layer_g_it_trun)
    for w in recentworkers:
        g_it = gradients[w]
        for layer_g_git, layer_g_t in zip(g_it,gradients_mean):
            layer_g_it_trun = layer_g_git[:-2]
            vargradient_element = torch.sub(layer_g_it_trun, layer_g_t)
            WithLateVar += torch.sum(vargradient_element**2).item()
    for grad_layer in gradients_mean:
        WithLateGradNormsquare += torch.sum(grad_layer.view(-1)**2).item()
       # print(gradients_mean)
    return WithLateGradNormsquare, WithLateVar,num_recentworker,gradients_mean


def Update(InforList,workers, gradients, gradients_to_add, t):
    k_t = len(workers)
    GradNormsquare = 0
    Var = 0
    count = 0
    for grad_layer in gradients_to_add:
        b = torch.sum(grad_layer.view(-1)**2).item()
        GradNormsquare += b
        count += 1
    count = 0
    for w in workers:
        g_it = gradients[w]
        iter_num = 0
        for layer_g_it, layer_g_t in zip(g_it,gradients_to_add):
            layer_g_it_trun = layer_g_it[:-2]
            vargradient_element = torch.sub(layer_g_it_trun, layer_g_t)
            b = torch.sum(vargradient_element**2).item()
            if iter_num == 0: Train_grad = layer_g_it_trun.view(-1)
            else: Train_grad = torch.cat((Train_grad, layer_g_it_trun.view(-1)))
            iter_num += 1
            Var += b
        Train_grad = torch.reshape(Train_grad,[1, Train_grad.shape[0]])
        if count ==0 : Grad_Matrix = Train_grad
        else : Grad_Matrix = torch.cat((Grad_Matrix, Train_grad))
        count += 1 
    stdlist = Grad_Matrix.std(dim=0)
    var2 = torch.sum(stdlist**2).item() #this has already considered k_t 
    if Var == 0: #  
        i = -1
        while InforList[i][1] ==1:
            i -= 1
        Var = InforList[i][2]/(InforList[i][1]-1) #this should be paid attention !!!!!!!!
    InforList.append((t,k_t,Var,GradNormsquare,copy.deepcopy(gradients_to_add)))
    return GradNormsquare, Var
 
